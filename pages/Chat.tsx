import React, { useEffect, useState, useRef } from 'react';
import tmi from 'tmi.js';

type ChatMessage = {
    username: string;
    message: string;
};

type ChatProps = {
    channelName: string;
};

const Chat: React.FC<ChatProps> = ({ channelName }) => {
    const [messages, setMessages] = useState<ChatMessage[]>([]);
    const chatRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const client = tmi.Client({
            channels: [channelName],
        });

        client.connect();

        const handleMessage = (
            channel: string,
            tags: tmi.ChatUserstate,
            message: string,
            self: boolean
        ) => {
            if (!self) {
                const newMessage: ChatMessage = {
                    username: tags.username || '',
                    message: message,
                };

                setMessages((prevMessages) => {
                    const existingMessages = prevMessages.map(
                        (msg) => msg.message
                    );
                    if (!existingMessages.includes(newMessage.message)) {
                        return [...prevMessages, newMessage];
                    }
                    return prevMessages;
                });
            }
        };

        client.on('message', handleMessage);

        return () => {
            client.disconnect();
        };
    }, [channelName]);

    const handleClearChat = () => {
        setMessages([]);
    };

    useEffect(() => {
        // Scroll to the bottom of the chat on message updates
        if (chatRef.current) {
            chatRef.current.scrollTop = chatRef.current.scrollHeight;
        }
    }, [messages]);

    return (
        <div className="flex flex-col h-64 bg-gray-800 rounded-lg p-4">
            <div className="flex items-center justify-between mb-4">
                <h2 className="text-lg font-medium text-white">
                    {channelName}&apos;s Chat
                </h2>
                <button
                    className="px-2 py-1 text-xs text-gray-300 bg-gray-700 rounded focus:outline-none"
                    onClick={handleClearChat}
                >
                    Clear Chat
                </button>
            </div>
            <div
                className="overflow-y-auto"
                ref={chatRef}
                style={{ maxHeight: 'calc(100% - 40px)' }}
            >
                {messages.map((message, index) => (
                    <div key={index} className="flex mb-2">
                        <span className="font-semibold text-purple-500">
                            {message.username}:{' '}
                        </span>
                        <span className="text-white">{message.message}</span>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Chat;
