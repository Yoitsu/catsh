import React from 'react';

const Banner = () => {
    const bannerStyles = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '200px',
        animation: 'rainbow 1s infinite linear',
    };
    const animationStyles = `
    @keyframes rainbow {
      0% {
        font-size: 64px;
        color: red;
      }
      25% {
        font-size: 96px;
        color: yellow;
      }
      50% {
        font-size: 128px;
        color: blue;
      }
      75% {
        font-size: 96px;
        color: green;
      }
      100% {
        font-size: 64px;
        color: red;
      }
    }
  `;

    return (
        <div style={bannerStyles}>
            <style>{animationStyles}</style>
            <h1 className="font-bold">CATSHIT EXPOSED!!!</h1>
        </div>
    );
};

export default Banner;
