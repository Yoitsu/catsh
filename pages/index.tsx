import React from 'react';
import fs from 'fs';
import type { InferGetServerSidePropsType, GetServerSideProps } from 'next';
import arrow from '../assets/arrow.svg';
import Banner from './Banner';
import Image from 'next/image';

type Props = string[];

const isDev = process.env.NODE_ENV === 'development';

export const getServerSideProps: GetServerSideProps<{
    images: Props;
}> = async () => {
    // Replace 'path/to/images' with the actual path to your folder of images
    const imageFolder = isDev
        ? '/home/canon/Pictures/catsh'
        : '/var/www/html/media';

    // Read the images from the folder
    const imageFiles = fs.readdirSync(imageFolder);
    const images = imageFiles.map((file) => `https://catsh.zip/media/${file}`);
    return { props: { images } };
};

const Arrow = () => {
    return (
        <div>
            <Image
                className="animate-bounce flex justify-self-center justify-center align-center place-self-center self-end"
                src={arrow}
                alt="catshit"
            />
        </div>
    );
};

const CollagePage = ({
    images,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
    return (
        <div className="bg-chatterino">
            <div className="py-20 m-1000">
                <Banner />
            </div>
            <div className="fixed top-0 right-0 bottom-0 flex justify-end items-center">
                <iframe
                    src="https://www.twitch.tv/embed/catsh/chat?parent=catsh.zip"
                    height="800"
                    width="400"
                    className="h-full"
                ></iframe>
            </div>
            <div className="flex flex-col items-center justify-center">
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <Arrow />
                <div className="h-screen" />
                <div className="">
                    <img src="https://catsh.zip/media/44dde616730975b458d5442907a6f45c39413bf03cb11ce6a3b92bbd0a0298e1.png" />
                </div>
            </div>
        </div>
    );
};

export default CollagePage;
