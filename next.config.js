// @ts-check

/**
 * @type {import('next').NextConfig}
 **/
const nextConfig = {
    reactStrictMode: true,
    experimental: {
        appDir: false,
    },
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: 'catsh.zip',
                port: '',
                pathname: '/media/**',
            },
        ],
    },
};

module.exports = nextConfig;
