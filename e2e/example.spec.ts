import { test, expect } from '@playwright/test';

test('should navigate to documentation', async ({ page }) => {
    await page.goto('/');
    await expect(page.locator('h1')).toContainText('Welcome to Next.js');
});
